//
//  SimpleModel.h
//  Segmented Control
//
//  Created by Joseph Kandi on 2013/03/07.
//  Copyright (c) 2013 Apps4u. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleModel : NSObject

@property (strong, nonatomic) NSArray *items;
@end
