//
//  ViewController.h
//  Segmented Control
//
//  Created by Joseph Kandi on 2013/03/07.
//  Copyright (c) 2013 Apps4u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleModel.h"

@interface ViewController : UIViewController

@end
