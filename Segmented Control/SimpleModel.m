//
//  SimpleModel.m
//  Segmented Control
//
//  Created by Joseph Kandi on 2013/03/07.
//  Copyright (c) 2013 Apps4u. All rights reserved.
//

#import "SimpleModel.h"

@implementation SimpleModel

-(NSArray *) items
{
    if (!_items) {
        _items = @[@"Model first value", @"Model second value"];
    }
return _items;
}
@end
