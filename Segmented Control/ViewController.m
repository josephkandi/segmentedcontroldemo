//
//  ViewController.m
//  Segmented Control
//
//  Created by Joseph Kandi on 2013/03/07.
//  Copyright (c) 2013 Apps4u. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) SimpleModel *model;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *selectedLabel;
@end

@implementation ViewController

-(SimpleModel *) model
{
    if (!_model) {
        _model = [[SimpleModel alloc] init];
    }
    return _model;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.segmentedControl addTarget:self action:@selector(pickOne:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) pickOne:(id) sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    self.selectedLabel.text = self.model.items[segmentedControl.selectedSegmentIndex];
}

@end
